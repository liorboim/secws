//
//  main.c
//  UserFw
//
//  Created by lior bomwurzel on 4/23/15.
//  Copyright (c) 2015 lior bomwurzel. All rights reserved.
//
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define LINE_LENGTH 100
#define SUCCESS 1
#define ERROR 0
const static char keren_file_rule_path[]= "/dev/fw_rules";
const static char keren_file_log_path[]= "/dev/fw_log";
const static char keren_file_conn_path[]= "/dev/fw_conn_tab";
const static char sys_active[]= "/sys/class/fw/fw_rules/active";
const static char sys_log_cler []= "/sys/class/fw/fw_log/log_clear";
const static char sys_rule_cler[]= "/sys/class/fw/fw_rules/rule_clear";
//writes from a file to the rule table in the module
int  write_rules_from_line( char* path){
    FILE *Kern_rules;
    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;
    path[strlen(path)-1]='\0';
    fp = fopen(path, "r");
    if (fp == NULL)
    {
        printf("cant open file\n");
        exit(EXIT_FAILURE);
    }
    Kern_rules = fopen(keren_file_rule_path, "r+");
    if (Kern_rules == NULL) {
        fprintf(stderr, "error open rule char device");
        return ERROR;
    }
    while((read = getline(&line, &len, fp)) != -1) {
        printf("Retrieved line of length %zu :\n", read);
        fwrite(line ,sizeof(char), read, Kern_rules);
    }
    fclose(Kern_rules);
    fclose(fp);
    if (line)
        free(line);
    return SUCCESS;
}
//gets the next place of space
char* get_space(char *line){
    char* space_place=strstr(line, " ");
    if (space_place==NULL)
        printf("second parameter is missing\n");
    return space_place;
}
//read line by line file
int read_file(const char *path){
    FILE *Kern_rules;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;
    Kern_rules = fopen(path, "r");
    if (Kern_rules == NULL) {
        fprintf(stderr, "error open file");
        return ERROR;
    }
    while((read = getline(&line, &len, Kern_rules)) != -1) {
        printf("%s", line);
    }
    fclose(Kern_rules);
    if (line)
        free(line);
    return SUCCESS;
}
//writes one char to file
int  sys_store_command(const char* path,char* c){
    FILE *Kern_rules;
    Kern_rules = fopen(path, "r+");
    if (Kern_rules == NULL) {
        fprintf(stderr, "error open rule char device");
        
        return ERROR;
    }
    fwrite(c,sizeof(char),1, Kern_rules);
    fclose(Kern_rules);
    return SUCCESS;
}
/**
 suport command
 firewall_activation <0 or 1>
 show_rule_table
 clear_rule_table
 load_rule_table_from_file <path>
 show_log
 show_connection_table
 clear_log*/
//if command is not valid  display error
int main(int argc, const char * argv[]) {
    char line[LINE_LENGTH];
    char * space_place;
    fgets (line, LINE_LENGTH, stdin);
    if (strstr(line, "load_rule_table") != NULL) {
        space_place=get_space(line);
        if(space_place==NULL)
            return ERROR;
        else
            return write_rules_from_line(space_place+1);
    }
    if (strstr(line, "show_rule_table") != NULL) {
        return read_file(keren_file_rule_path);
    }
    if (strstr(line, "show_log") != NULL) {
        return read_file(keren_file_log_path);
    }
    if (strstr(line, "show_connection_table") != NULL) {
        return read_file(keren_file_conn_path);
    }
    if (strstr(line, "clear_rule_table") != NULL) {
        return sys_store_command(sys_rule_cler,"c");
    }
    if (strstr(line, "clear_log") != NULL) {
        return sys_store_command(sys_log_cler,"c");
    }
    if (strstr(line, "firewall_activation") != NULL) {
        space_place=get_space(line);
        if(space_place==NULL)
            return ERROR;
        else
            if (strstr(space_place+1, "0") != NULL) {
                return  sys_store_command(sys_active,"0");
            }
        if (strstr(space_place+1, "1") != NULL) {
            return  sys_store_command(sys_active,"1");
        }
    }
    fprintf(stderr, "not a valid command");
    return ERROR;
}