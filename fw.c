#undef __KERNEL__
#define __KERNEL__
#undef __MODULE__
#define __MODULE__
#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/time.h>
#include "fw.h"
#define SUCCESS 0
#define ERROR -1
#define CONTINUE_CHECKING 9
#define BASIC_RULE -40
#define FIN -41
#define FTP_PROT -9
#define HTTP_PROT -10
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Bomwurzel Lior");
MODULE_DESCRIPTION("Filter packets by type");
static struct nf_hook_ops nfLoacl_out;
static  rule_t *rule_table;
static int num_of_rules=0;
static int rules_read=1;
static int logs_read=0;
static log_row_t *row;
static int next_row_index=0;
static int size_row_table=10;
static  int buf_place=0;
static int minor_rule;/* rule device minor number */
static int major_rule; /* rule device major number */
static int minor_log;/*log device minor number */
static int major_log; /*log device major number */
static int minor_conn;/*log device minor number */
static int major_conn; /*log device major number */
static struct class * my_class;
static struct device * my_device_rule;
static struct device * my_device_log;
static struct device * my_device_conn;

static int active_state=0;
static rule_t *rule_table;
//functions
static int print_connection(char Message[],log_conn_t *conn);
void destroy_connection_list(void);
static void clean_all(void);
static int fill_rules(char * Message);
static int get_rule(int i,char Message []);
static int print_row(char Message[],int i);
static void nf_register_hook_local_out(void);
static log_row_t* generate_log_row(struct sk_buff *skb,unsigned int hooknum);
static int weight_known_pattern(char * data,int start,int end);
static list_conn_t* list_conn;
static log_conn_t* conn_read;

//device fw_rule function goes on the rule table and print it to user space
static ssize_t device_read(struct file *file, /* see include/linux/fs.h   */
                           char __user * buffer, /* buffer to be filled with data */
                           size_t length,  /* length of the buffer     */
                           loff_t * offset)

{
    char *Message_Ptr;
    int bytes_read = 0;
    char Message [100];
    int i;
    if(num_of_rules<=rules_read)
    { //finish read
        rules_read=1;
        return 0;
    }
    i=rules_read;
    for(;i<num_of_rules;i++)
    {  get_rule(i,Message);
        Message_Ptr=(char*)&Message;
        while (length && *Message_Ptr) {
            put_user(*(Message_Ptr++), buffer++);
            length--;
            bytes_read++;
        }
    }
    rules_read=i;
    
    return  bytes_read;
    
    
    
}
//fill the basic rule to the rule table.
// this rule will appear to user in the show_rule_table function but not in the show _log
int fill_basic_rule(void)
{
    int retval;
    char Message[80];
    sprintf(Message,"mydif 3 127.0.0.1/8 127.0.0.1/8 143 0 0 3 1");
    retval=fill_rules(Message);
    if(retval==ERROR){clean_all(); return ERROR;}
    else num_of_rules++;
    return SUCCESS;
}
//device fw_rule write, get from user space the rules and fills it to the rule_table
static ssize_t device_write(struct file *file,
                            const char __user * buffer, size_t length, loff_t * offset)

{  char Message[80];
    int Message_place;
    int retval;
    num_of_rules=1;
    //read from file, each new line fill new rule
    for ( Message_place = 0; Message_place < length  && buffer[buf_place];Message_place++, buf_place++)
    {
        get_user(Message[Message_place], buffer + buf_place);
        if(Message[Message_place]=='\n')
        {
            Message[Message_place]='\0';
            if (Message_place!=0) {
                Message_place=-1;
                retval=fill_rules(Message);
                
                if(retval==ERROR){clean_all(); return ERROR;}
                else
                    if ( num_of_rules<MAX_RULES)
                        num_of_rules++;
                    else {printk(KERN_ERR "too many rules ,kernel is going down"); clean_all(); return ERROR;}
            }
        }
    }
    retval=buf_place;
    if(!buffer[buf_place])
    {  //end of write
        buf_place=0;
    }
    
    
    return  retval;
}

//struct of functions for fw_rules devcie
struct file_operations Fops = {
    .read = device_read,
    .write = device_write,
};
//device fw_log read functions gos on the row array and prints its values to the user space
static ssize_t device_read_conn(struct file *file, /* see include/linux/fs.h   */
                                char __user * buffer, /* buffer to be filled with data */
                                size_t length,  /* length of the buffer     */
                                loff_t * offset)

{
    char *Message_Ptr;
    ssize_t bytes_read = 0;
    char Message [1000];
    
    if(conn_read==NULL)
    {
        //finish read
        conn_read=list_conn->head;
        return 0;
    }
    
    while(conn_read!=NULL)
        
    {
        print_connection(Message,conn_read);
        Message_Ptr=(char*)&Message;
        while (length && *Message_Ptr) {
            put_user(*(Message_Ptr++), buffer++);
            length--;
            bytes_read++;
        }
        conn_read=conn_read->next;
    }
    
    return  bytes_read;
}

//struct of functions for device fw_log
struct file_operations Fops_conn = {
    .read = device_read_conn,
};


//device fw_log read functions gos on the row array and prints its values to the user space
static ssize_t device_read_log(struct file *file, /* see include/linux/fs.h   */
                               char __user * buffer, /* buffer to be filled with data */
                               size_t length,  /* length of the buffer     */
                               loff_t * offset)

{
    char *Message_Ptr;
    ssize_t bytes_read = 0;
    char Message [1000];
    int i;
    if(logs_read>=next_row_index)
    { //finish read
        logs_read=0;
        return logs_read;
    }
    i=logs_read;
    for(;i<next_row_index;i++)
    {  print_row(Message,i);
        Message_Ptr=(char*)&Message;
        while (length && *Message_Ptr) {
            put_user(*(Message_Ptr++), buffer++);
            length--;
            bytes_read++;
        }
    }
    logs_read=i;
    return  bytes_read;
}
//struct of functions for device fw_log
struct file_operations Fops_log = {
    .read = device_read_log,
};




//attributes for the /sys/classes/fw/fw_rules
//show the active state of the fw
static ssize_t show_active(struct  device * dev, struct device_attribute * attr, char * buffer){
    
    return sprintf(buffer, "%d\n",active_state);
}
//change the active state of the fw
static ssize_t store_active(struct  device * dev, struct device_attribute * attr,
                            
                            const char * buffer, size_t size){
    
    
    if ((strcmp(buffer, "1") == 0))
        
    {
        if (active_state==1)
            printk(KERN_ERR "firewall already active\n");
        else {
            active_state=1;
        }
    }
    if ((strcmp(buffer, "0") == 0))
    {
        if (active_state==0)
            printk(KERN_ERR "firewall already not active\n");
        else {
            active_state=0;
        }
    }
    return size;
}

//clear the rule table
static ssize_t store_rules(struct  device * dev, struct device_attribute * attr,
                           const char * buffer, size_t size){
    if(strlen(buffer)==1)
    {
        num_of_rules=1;//keep the basic rule
    }
    else
        printk(KERN_ERR "only one char is premited");
    return size;
}
//display number of rules
static ssize_t show_rules(struct  device * dev, struct device_attribute * attr, char * buffer){
    return sprintf(buffer, "%d\n", num_of_rules-1);
}


//attributes for the /sys/classes/fw/fw_log
//display amount of logs
static ssize_t show_logs(struct  device * dev, struct device_attribute * attr, char * buffer){
    return sprintf(buffer, "%d\n", next_row_index);
    
}
//clear the connection table
static ssize_t store_conn(struct  device * dev, struct device_attribute * attr,
                          
                          const char * buffer, size_t size){
    if(strlen(buffer)==1)
    {
        destroy_connection_list();
    }
    else
        printk(KERN_ERR "only one char is premited");
    return size;
}

//clear the logs
static ssize_t store_logs(struct  device * dev, struct device_attribute * attr,
                          
                          const char * buffer, size_t size){
    if(strlen(buffer)==1)
    {
        next_row_index=0;
    }
    else
        printk(KERN_ERR "only one char is premited");
    return size;
}


/*macro helper for defining device attributes,register attr_show and attr_store to "fw" sysfs file */


static DEVICE_ATTR(active, S_IWUGO | S_IRUGO, show_active, store_active);

static DEVICE_ATTR(rules_size, S_IWUGO | S_IRUGO, show_rules,NULL);

static DEVICE_ATTR(log_size, S_IWUGO | S_IRUGO, show_logs,NULL);

static DEVICE_ATTR(log_clear, S_IWUGO | S_IRUGO, NULL ,store_logs);

static DEVICE_ATTR(conn_clear, S_IWUGO | S_IRUGO, NULL ,store_conn);

static DEVICE_ATTR(rule_clear, S_IWUGO | S_IRUGO, NULL ,store_rules);

//register the devices fw_log and fw_rules,
//register the attributes fw_rules :active,rules_clear,rule_size fw_log: log_clear log size
static int register_sysfs_device(void){
    
    int errno1;
    int errno2;
    int errno3;
    int errno4;
    int errno5;
    int errno6;
    dev_t devt;
    dev_t devt_log;
    dev_t devt_conn;
    major_rule = register_chrdev(0, DEVICE_NAME_RULES, &Fops);
    major_log = register_chrdev(0,DEVICE_NAME_LOG, &Fops_log);
    major_conn = register_chrdev(0,DEVICE_NAME_CONN_TAB, &Fops_conn);
    if (major_rule < 0 ||major_log<0||major_conn<0)
        
    {
        
        printk(KERN_ERR "faild to register device\n");
        return ERROR;
        
    }
    /* register class */
    
    my_class = class_create(THIS_MODULE,CLASS_NAME);
    
    if (my_class == NULL)
        
        
        
    {
        unregister_chrdev(major_rule, DEVICE_NAME_RULES);
        unregister_chrdev(major_conn, DEVICE_NAME_CONN_TAB);
        unregister_chrdev(major_log, DEVICE_NAME_LOG);
        
        printk(KERN_ERR " failed to create class\n");
        
        return ERROR;
        
    }
    
    /* creat device*/
    
    devt = MKDEV(major_rule, minor_rule);
    devt_log = MKDEV(major_log, minor_log);
    devt_conn=MKDEV(major_conn, minor_conn);
    my_device_rule = device_create(my_class, NULL, devt, NULL, DEVICE_NAME_RULES);
    my_device_log = device_create(my_class, NULL, devt_log, NULL, DEVICE_NAME_LOG);
    my_device_conn = device_create(my_class, NULL, devt_conn, NULL, DEVICE_NAME_CONN_TAB);
    if (my_device_rule == NULL|| my_device_log==NULL||my_device_conn==NULL)
    {
        class_destroy(my_class);
        unregister_chrdev(major_rule, DEVICE_NAME_RULES);
        unregister_chrdev(major_log, DEVICE_NAME_LOG);
        printk( "faild to creat device\n");
        return ERROR;
    }
    /*creat sysfs files*/
    errno1 = device_create_file(my_device_rule, &dev_attr_rules_size);
    errno2 = device_create_file(my_device_rule, &dev_attr_active);
    errno3 = device_create_file(my_device_log, &dev_attr_log_size);
    errno4 = device_create_file(my_device_log, &dev_attr_log_clear);
    errno5= device_create_file(my_device_rule, &dev_attr_rule_clear);
    errno6 = device_create_file(my_device_conn, &dev_attr_conn_clear);
    
    if (errno1 < 0 || errno2<0 ||errno3 <0 || errno4 <0|| errno5 <0)
        
    {
        device_destroy(my_class, devt);
        device_destroy(my_class, devt_log);
        device_destroy(my_class, devt_conn);
        class_destroy(my_class);
        unregister_chrdev(major_rule, DEVICE_NAME_RULES);
        unregister_chrdev(major_log, DEVICE_NAME_LOG);
        unregister_chrdev(major_log, DEVICE_NAME_CONN_TAB);
        printk(KERN_ERR "Failed to create a device file.\n");
        return ERROR;
        
    }
    printk("success inserting device to kernel\n");
    return SUCCESS;
}



/*this function destroy and unregisteres our device and class*/
static void unregister_sysfs_device(void){
    device_remove_file(my_device_rule, &dev_attr_active);
    device_remove_file(my_device_log, &dev_attr_log_clear);
    device_remove_file(my_device_conn, &dev_attr_conn_clear);
    device_remove_file(my_device_log, &dev_attr_log_size);
    device_remove_file(my_device_rule, &dev_attr_rules_size);
    device_remove_file(my_device_rule, &dev_attr_rule_clear);
    device_destroy(my_class, MKDEV(major_rule, minor_rule));
    device_destroy(my_class, MKDEV(major_log, minor_log));
    device_destroy(my_class, MKDEV(major_conn, minor_conn));
    class_destroy(my_class);
    unregister_chrdev(major_rule, DEVICE_NAME_RULES);
    unregister_chrdev(major_log, DEVICE_NAME_LOG);
    printk("unregister sysfs device\n");
    
    
    
}

//fill the tcp heders, if the fin psh fill the tcp_flags filed with REASON_XMAS_PACKET value else fill the flag with the ack real flag
static int get_tcp_headers(struct sk_buff *skb,struct iphdr *ip_header,log_row_t * new_row){
    struct tcphdr *tcp_header;
    tcp_header = (struct tcphdr *)(skb->data +(ip_header->ihl * 4));
    if(!tcp_header) return ERROR;
    (new_row)->dst_port=ntohs(tcp_header->dest);
    (new_row)->src_port=ntohs(tcp_header->source);
    //check for Xmas packet
    if((tcp_header->fin)&&(tcp_header->psh)&&(tcp_header->urg))
        (new_row)->tcp_flags=REASON_XMAS_PACKET;
    else
        if (tcp_header->fin)
        {
            (new_row)->tcp_flags=FIN;
        }
        else//regular tcp get ack
        {
            (new_row)->tcp_flags=tcp_header->ack+1;
            (new_row)->syn=tcp_header->syn;
        }
    return SUCCESS;
}
//fill the udp haders
static int get_udp_headers(struct sk_buff *skb,struct iphdr *ip_header,log_row_t *new_row){
    struct udphdr *udp_header;
    udp_header = (struct udphdr *)skb_transport_header(skb);
    if(!udp_header) return ERROR;
    (new_row)->dst_port=ntohs(udp_header->dest);
    (new_row)->src_port=ntohs(udp_header->source);
    
    return SUCCESS;
}
//fill all the ditails of the paket like timestamp src dst protocol..
//put the details in tha row[next_row_index]
static log_row_t* generate_log_row(struct sk_buff *skb,unsigned int hooknum)
{
    log_row_t *new_row;
    struct timeval timestamp;
    int retval;
    struct iphdr *ip_header;
    retval=SUCCESS;
    if (!skb )return SUCCESS;
    new_row = kmalloc(sizeof(log_row_t), GFP_ATOMIC);
    if (new_row==NULL) {
        return NULL;
    }
    do_gettimeofday(&timestamp);
    (new_row)->timestamp=timestamp.tv_sec;
    (new_row)->tcp_flags=ACK_ANY;
    (new_row)->dst_port=0;
    (new_row)->src_port=0;
    (new_row)->syn=0;
    ip_header = (struct iphdr *)skb_network_header(skb);
    (new_row)->src_ip=ntohl(ip_header->saddr);
    (new_row)->dst_ip=ntohl(ip_header->daddr);
    (new_row)->protocol=ip_header->protocol;
    if ( (new_row)->protocol== IPPROTO_TCP) {
        retval=get_tcp_headers(skb,ip_header,new_row);
    }
    if ( (new_row)->protocol== IPPROTO_UDP) {
        retval=get_udp_headers(skb,ip_header,new_row);
    }
    if(retval==ERROR)
    {
        return NULL;
    }
    (new_row)->hooknum=hooknum;
    return new_row;
}
//ceck if packet surce and dst mech the rule i in the table
static int chek_row_with_rule(int i,const struct net_device *in,const struct net_device *out,log_row_t *new_row){
    __be32 mask;
    __be32 ip_row;
    __be32 ip_rule;
    int packet_direction;
    packet_direction=DIRECTION_ANY;
    //check direction
    if(in!=NULL&&out!=NULL)
    {
        if ((strcmp(in->name,IN_NET_DEVICE_NAME)==0)&&(strcmp(out->name,OUT_NET_DEVICE_NAME)==0)){
            packet_direction=DIRECTION_OUT;
            
        }
        if((strcmp(out->name,IN_NET_DEVICE_NAME)==0)&&(strcmp(in->name,OUT_NET_DEVICE_NAME)==0)){
            packet_direction=DIRECTION_IN;
            
        }
        if (!((packet_direction)&((rule_table+i)->direction))) {
            return CONTINUE_CHECKING;
        }
    }
    
    
    //check src ip
    mask=(rule_table+i)->src_prefix_mask;
    ip_row=(new_row)->src_ip;
    ip_rule=(rule_table+i)->src_ip;
    
    if(ip_rule!=0)
        if ((ip_rule&mask)!=(ip_row&mask)) {
            return CONTINUE_CHECKING;
        }
    
    //check dst ip
    mask=(rule_table+i)->dst_prefix_mask;
    ip_row=(new_row)->dst_ip;
    ip_rule=(rule_table+i)->dst_ip;
    if(ip_rule!=0)
        if ((ip_rule&mask)!=(ip_row&mask)) {
            return CONTINUE_CHECKING;
        }
    
    //check protocol
    if((rule_table+i)->protocol!=PROT_ANY)
        if((new_row)->protocol!=(rule_table+i)->protocol)
            return CONTINUE_CHECKING;
    
    //check src port
    if((rule_table+i)->src_port!=PORT_ANY)
        if((rule_table+i)->src_port<PORT_ABOVE_1023 &&(new_row)->src_port<PORT_ABOVE_1023)
            if((new_row)->src_port!=(rule_table+i)->src_port)
                return CONTINUE_CHECKING;
    
    //check src port
    if((rule_table+i)->dst_port!=PORT_ANY)
        if((rule_table+i)->dst_port<PORT_ABOVE_1023 &&(new_row)->dst_port<PORT_ABOVE_1023)
            if((new_row)->dst_port!=(rule_table+i)->dst_port)
                return CONTINUE_CHECKING;
    
    //check ack
    if((rule_table+i)->ack!=ACK_ANY)
        if((new_row)->tcp_flags!=(rule_table+i)->ack)
            return CONTINUE_CHECKING;
    
    return SUCCESS ;
}
//make sure the array is not full
static int fill_new_row_in_table(log_row_t* new_row){
    log_row_t* tmp_rows;
    //if new allocation nedded
    if (next_row_index>=size_row_table) {
        size_row_table*=2;
        tmp_rows=krealloc(row, size_row_table*sizeof(log_row_t), GFP_ATOMIC);
        if (tmp_rows!=NULL) {
            row=tmp_rows;
        }
        else {
            kfree (row);
            printk("Error (re)allocating memory");
            return ERROR;
        }
    }
    (row+next_row_index)->timestamp =(new_row)->timestamp;
    (row+next_row_index)->protocol=(new_row)->protocol;
    (row+next_row_index)->action =(new_row)->action;
    (row+next_row_index)->hooknum=(new_row)->hooknum;
    (row+next_row_index)->src_ip =(new_row)->src_ip;
    (row+next_row_index)->dst_ip=(new_row)->dst_ip;
    (row+next_row_index)->src_port=(new_row)->src_port;
    (row+next_row_index)->dst_port=(new_row)->dst_port;
    (row+next_row_index)->reason=(new_row)->reason;
    (row+next_row_index)->count=(new_row)->count;
    (row+next_row_index)->tcp_flags=(new_row)->tcp_flags;
    (row+next_row_index)->syn=(new_row)->syn;
    next_row_index++;
    return SUCCESS;
}

//if not update relevant row,
//if yes next_row_index and make sure the array is not full
static int is_row_new(log_row_t* new_row){
    int i;
    for (i=0; i<next_row_index; i++) {
        if(
           
           (new_row)->protocol ==(row+i)->protocol &&
           (new_row)->action ==(row+i)->action     &&
           (new_row)->hooknum ==(row+i)->hooknum   &&
           (new_row)->src_ip ==(row+i)->src_ip     &&
           (new_row)->dst_ip ==(row+i)->dst_ip     &&
           (new_row)->src_port ==(row+i)->src_port &&
           (new_row)->dst_port ==(row+i)->dst_port &&
           (new_row)-> reason==(row+i)->reason
           
           )
        {
            //row isnt new ,update old
            (row+i)->count++;
            (row+i)->timestamp=(new_row)->timestamp;
            return SUCCESS;
            
        }
    }
    //row new
    (new_row)->count=1;
    fill_new_row_in_table(new_row);
    return SUCCESS;
}
//if return CONTINUE_CHECKING means no found in the baisi inspection
static __u8 inspect_row_basic(const struct net_device *in,const struct net_device *out,log_row_t* new_row){
    __u8 check_result;
    
    //check basic rule
    (new_row)->reason=0;
    (new_row)->action=NF_DROP;
    check_result=chek_row_with_rule(0,in,out,new_row);
    if(check_result!=CONTINUE_CHECKING)
    {     (new_row)->reason=BASIC_RULE;//row found in rules, reason is number of rule
        return  NF_ACCEPT;
    }
    //check if fw is active
    if(active_state==0)
    {
        (new_row)->reason=REASON_FW_INACTIVE;
        return NF_ACCEPT;
    }
    //check XMAS_PACKET
    if ((new_row)->tcp_flags==REASON_XMAS_PACKET) {
        (new_row)->reason=REASON_XMAS_PACKET;
        return NF_DROP;
    }
    
    return NF_DROP;
}

//packet that reached hear means that ack=0,inspect in the static table,if the packet is also tcp it will be send to another function to deal with new connection
//decide what to do with the packet that is now on inspection, and fill the reason of and the action in accordance with the decision
static __u8  inspect_row_static(const struct net_device *in,const struct net_device *out ,log_row_t * new_row){
    __u8 check_result;
    int i;
    for(i=1,check_result=CONTINUE_CHECKING;i<num_of_rules;i++){
        check_result=chek_row_with_rule(i,in,out,new_row);
        if(check_result!=CONTINUE_CHECKING)
        {     ( new_row)->reason=i-1;//row found in rules, reason is number of rule
            (new_row)->action= (rule_table+i)->action;
            return (new_row)->action;
        }
    }
    //rule not found
    (new_row)->reason=REASON_NO_MATCHING_RULE;
    (new_row)->action= NF_DROP;
    return (new_row)->action;
}
//return the pointer to the next string of the row and put  \0 insted of space
static char* get_next_string(char *Message,char delimit[]){
    char *temp_str;
    temp_str=strstr(Message,delimit);
    if(temp_str==NULL)
        return NULL;
    *(temp_str)='\0';
    return temp_str+1;
}
//extract an ip address from string
unsigned int inet_addr(char *str)
{
    int a,b,c,d;
    char arr[4];
    sscanf(str,"%d.%d.%d.%d",&a,&b,&c,&d);
    arr[3] = a; arr[2] = b; arr[1] = c; arr[0] = d;
    return *(unsigned int*)arr;
}
//prints on Message the rule[i] details
static int get_rule(int i,char Message []){
    int place;
    __be32 ip;
    //name
    place=sprintf(Message,"%s ",(rule_table+i)->rule_name );
    //direction
    place+=sprintf(Message+place,"%s ",direction_t_string[(rule_table+i)->direction]);
    //src ip
    if((rule_table+i)->src_ip ==0&&(rule_table+i)->src_prefix_size==0)
        place+=sprintf(Message+place,"%s","any ");
    else
    { ip=htonl((rule_table+i)->src_ip);
        place+=sprintf(Message+place,"%pI4/%d ",&ip,(rule_table+i)->src_prefix_size);
    }
    //dst ip
    if((rule_table+i)->dst_ip ==0&&(rule_table+i)->dst_prefix_size==0)
        place+=sprintf(Message+place,"%s","any ");
    else
    {
        ip=htonl((rule_table+i)->dst_ip);
        place+=sprintf(Message+place,"%pI4/%d ",&ip ,(rule_table+i)->dst_prefix_size);
        
    }    //protocol
    switch ((rule_table+i)->protocol) {
        case PROT_ICMP:
            place+=sprintf(Message+place,"%s","ICMP ");
            break;
        case PROT_TCP:
            place+=sprintf(Message+place,"%s","TCP ");
            break;
        case PROT_UDP:
            place+=sprintf(Message+place,"%s","UDP ");
            break;
        case PROT_ANY:
            place+=sprintf(Message+place,"%s","any ");
            break;
        default:
            place+=sprintf(Message+place,"%s","OTHER ");
            break;
    }//src_port
    if((rule_table+i)->src_port ==PORT_ANY)
        place+=sprintf(Message+place,"%s","any ");
    if ((rule_table+i)->src_port >=PORT_ABOVE_1023)
        place+=sprintf(Message+place,"%s",">1023 ");
    if((rule_table+i)->src_port <PORT_ABOVE_1023 && (rule_table+i)->src_port >PORT_ANY)
        place+=sprintf(Message+place,"%d ",(rule_table+i)->src_port);
    //dst_port
    if((rule_table+i)->dst_port==PORT_ANY)
        place+=sprintf(Message+place,"%s","any ");
    if ((rule_table+i)->dst_port>=PORT_ABOVE_1023)
        place+=sprintf(Message+place,"%s",">1023 ");
    if((rule_table+i)->dst_port<PORT_ABOVE_1023 && (rule_table+i)->dst_port>PORT_ANY)
        place+=sprintf(Message+place,"%d ",(rule_table+i)->dst_port);
    //ack
    place+=sprintf(Message+place,"%s ",ack_t_string[(rule_table+i)->ack]);
    //action
    place+=sprintf(Message+place,"%s\n",NF_t_string[(rule_table+i)->action]);
    Message[place+1]='\0';
    return SUCCESS;
}

//fill rule_table[num_of_rules] with Message which is a rule in a string form
static int fill_rules(char *Message){
    int i=0;
    char *next_msg;
    unsigned long t;
    //name
    next_msg=get_next_string(Message," ");
    if(next_msg==NULL)
        return ERROR;
    strncpy((rule_table+num_of_rules)->rule_name,Message,20);
    //direction
    Message=next_msg;
    next_msg=get_next_string(Message," ");
    if(next_msg==NULL)
        return ERROR;
    i=strict_strtoul(Message,10,&t);
    if(i!=0)
        return ERROR;
    (rule_table+num_of_rules)->direction=t;
    //surch ip
    Message=next_msg;
    next_msg=get_next_string(Message,"/");
    if(next_msg==NULL)
        return ERROR;
    (rule_table+num_of_rules)->src_ip=inet_addr(Message);
    //src mask
    Message=next_msg;
    next_msg=get_next_string(Message," ");
    if(next_msg==NULL)
        return ERROR;
    i=strict_strtoul(Message,10,&t);
    if(i!=0) return ERROR;
    (rule_table+num_of_rules)->src_prefix_size=t;
    (rule_table+num_of_rules)->src_prefix_mask= (0xffffffff >> (32 - t )) << (32 - t);
    //dst ip
    Message=next_msg;
    next_msg=get_next_string(Message,"/");
    if(next_msg==NULL)
        return ERROR;
    (rule_table+num_of_rules)->dst_ip=inet_addr(Message);
    //dst mask
    Message=next_msg;
    next_msg=get_next_string(Message," ");
    if(next_msg==NULL)
        return ERROR;
    i=strict_strtoul(Message,10,&t);
    if(i!=0) return ERROR;
    (rule_table+num_of_rules)->dst_prefix_size=t;
    (rule_table+num_of_rules)->dst_prefix_mask= (0xffffffff >> (32 - t )) << (32 - t);
    //protocol
    Message=next_msg;
    next_msg=get_next_string(Message," ");
    if(next_msg==NULL)
        return ERROR;
    i=strict_strtoul(Message,10,&t);
    if(i!=0)
        return ERROR;
    ((rule_table+num_of_rules)->protocol)=(__u8)t;
    //src port
    Message=next_msg;
    next_msg=get_next_string(Message," ");
    if(next_msg==NULL)
        return ERROR;
    i=strict_strtoul(Message,10,&t);
    if(i!=0)
        return ERROR;
    ((rule_table+num_of_rules)->src_port)=(__be16)t;
    //dst port
    Message=next_msg;
    next_msg=get_next_string(Message," ");
    i=strict_strtoul(Message,10,&t);
    if(i!=0)
        return ERROR;
    ((rule_table+num_of_rules)->dst_port)=(__be16)t;
    //ack
    Message=next_msg;
    next_msg=get_next_string(Message," ");
    if(next_msg==NULL)
        return ERROR;
    i=strict_strtoul(Message,10,&t);
    if(i!=0)
        return ERROR;
    ((rule_table+num_of_rules)->ack)=(__u8)t;
    //action
    Message=next_msg;
    i=strict_strtoul(Message,10,&t);
    if(i!=0)
        return ERROR;
    ((rule_table+num_of_rules)->action)=(__u8)t;
    
    return SUCCESS;
}
//print Message row[i] which is a log of a packet
static int print_connection(char Message[],log_conn_t *conn)
{
    
    int place;
    __be32 ip;
    ip=ntohl(conn->src_ip);
    place=sprintf(Message,"%pI4 ", &ip);
    place+=sprintf(Message+place,"%d ", conn->src_port);
    ip=ntohl(conn->dst_ip);
    place+=sprintf(Message+place,"%pI4 ", &ip);
    place+=sprintf(Message+place,"%d ", conn->dst_port);
    place+=sprintf(Message+place,"%d ", conn->protocol);
    place+=sprintf(Message+place,"%d \n", conn->state);
    return SUCCESS;
}

//print Message row[i] which is a log of a packet
static int print_row(char Message[],int i)
{
    
    int place;
    __be32 ip;
    place=sprintf(Message,"%lu ",(row+i)->timestamp);
    place+=sprintf(Message+place,"%d ",(row+i)->count);
    ip=ntohl((row+i)->src_ip);
    place+=sprintf(Message+place,"%pI4 ", &ip);
    place+=sprintf(Message+place,"%d ", (row+i)->src_port);
    ip=ntohl((row+i)->dst_ip);
    place+=sprintf(Message+place,"%pI4 ", &ip);
    place+=sprintf(Message+place,"%d ", (row+i)->dst_port);
    place+=sprintf(Message+place,"%d ", (row+i)->protocol);
    place+=sprintf(Message+place,"%d ", (row+i)->hooknum);
    place+=sprintf(Message+place,"%d ", (row+i)->action);
    place+=sprintf(Message+place,"%d \n", (row+i)->reason);
    return SUCCESS;
}


log_conn_t* find_ftp_conn( int port){
    
    log_conn_t* conn= list_conn->head;
    while(conn!=NULL){
        if(conn->src_port==port || conn->dst_port==port)
            return conn;
        else
            conn=conn->next;
    }
    return NULL;
}

//insert conneciton at the end of the list
static log_conn_t* insert_new_connection(log_conn_t* new_conn)
{
    //for reading (this is the head of the list)
    if(list_conn->head==NULL)
    { conn_read=new_conn;
        list_conn->tail=new_conn;
        list_conn->head=new_conn;
    }
    //insert connection to table
    else{ (list_conn->tail)->next=new_conn;
        new_conn->prev=list_conn->tail;
        list_conn->tail=new_conn;
    }
    
    return new_conn;
    
}

//return null on error and new connection on success , put the new connection in the list
static log_conn_t* generate_new_ftp_connection(log_row_t * new_row,int port){
    
    struct timeval timestamp;
    log_conn_t* new_conn;
    //if ftp connection already try to connect
    //do not duplicate connections of ftp
    new_conn=find_ftp_conn(port);
    if(new_conn!=NULL)
        return new_conn;
    new_conn = kmalloc(sizeof(log_conn_t), GFP_ATOMIC);
    if(new_conn==NULL)
        return NULL;
    new_conn->next=NULL;
    new_conn->prev=NULL;
    do_gettimeofday(&timestamp);
    (new_conn)->timestamp=timestamp.tv_sec;
    (new_conn)->protocol=FTP_PROT;
    (new_conn)->state=READY_TO_NEW;
    new_conn->src_ip=(new_row)->dst_ip;
    new_conn->dst_ip=(new_row)->src_ip;
    new_conn->src_port=20;
    new_conn->dst_port=port;
    
    return insert_new_connection(new_conn);
    
}


//return null on erro and new connection on success , put the new connection in the list
static log_conn_t* generate_new_connection(log_row_t * new_row){
    
    struct timeval timestamp;
    log_conn_t* new_conn;
    
    new_conn = kmalloc(sizeof(log_conn_t), GFP_ATOMIC);
    if(new_conn==NULL)
        return NULL;
    new_conn->next=NULL;
    new_conn->prev=NULL;
    do_gettimeofday(&timestamp);
    (new_conn)->timestamp=timestamp.tv_sec;
    (new_conn)->protocol=PROT_TCP;
    (new_conn)->state=TCP_SYN;
    new_conn->src_ip=(new_row)->src_ip;
    new_conn->dst_ip=(new_row)->dst_ip;
    new_conn->src_port=(new_row)->src_port;
    new_conn->dst_port=(new_row)->dst_port;
    
    return insert_new_connection(new_conn);
    
}
//return null on error and new connection on success , put the new connection in the list
static log_conn_t* generate_new_http_connection(log_row_t * new_row,char * Message){
    
    
    int dst_ip=0;
    char *next_msg;
    int i;
    int j;
    unsigned long dst_port;
    struct timeval timestamp;
    log_conn_t* new_conn;
    //name
    next_msg=get_next_string(Message,":");
    if(next_msg==NULL)
        return NULL;//TODO return error
    dst_ip=inet_addr(Message);
    Message=next_msg;
    j=1;
    i=1;
    while(i!=0)
    {//get only the port form the ask
        i=strict_strtoul(Message,10,&dst_port);
        Message[strlen(Message)-j]='\0';
        j++;
    }
    new_conn = kmalloc(sizeof(log_conn_t), GFP_ATOMIC);
    if(new_conn==NULL)
        return NULL;
    new_conn->next=NULL;
    new_conn->prev=NULL;
    do_gettimeofday(&timestamp);
    (new_conn)->timestamp=timestamp.tv_sec;
    (new_conn)->protocol=HTTP_PROT;
    (new_conn)->state=READY_TO_NEW;
    new_conn->src_ip=0;
    new_conn->dst_ip=dst_ip;
    new_conn->src_port=0;
    new_conn->dst_port=dst_port;
    
    return insert_new_connection(new_conn);
    
}
//delete connection from the list of the open connections
static void delete_connection(log_conn_t * conn){
    
    
    if(conn->next!=NULL &&conn->prev!=NULL)
    {//conn is midle
        conn->prev->next=conn->next;
        conn->next->prev=conn->prev;
        
    }
    
    if(conn->next==NULL&&conn->prev==NULL)
    {//conn is the only node
        list_conn->tail=NULL;
        list_conn->head=NULL;
        conn_read=NULL;
    }
    if(conn->next==NULL&&conn->prev!=NULL)
    {//conn is last node but not the only one
        conn->prev->next=NULL;
        list_conn->tail=conn->prev;
        
    }
    if(conn->next!=NULL&&conn->prev==NULL)
    {//conn is first node but not the only one
        conn->next->prev=NULL;
        list_conn->head=conn->next;
        conn_read= list_conn->head;
        
    }
    
    kfree(conn);
    
}


//after the connection finished handshake , the connection is defined as valid and will be stay open until fin packet. if time stamp is 0 means the connection is open. web search connection that make the packet port and ip
static log_conn_t * find_conn_in_table_dynamic (log_row_t * new_row){
    log_conn_t *temp;
    temp=list_conn->head;
    while(temp!=NULL)
    {   //this mean we are stil on handshake
        if(temp->timestamp==0)
        {
            if ((temp->src_ip==(new_row)->src_ip&&
                 temp->dst_ip==(new_row)->dst_ip&&
                 temp->src_port==(new_row)->src_port&&
                 temp->dst_port==(new_row)->dst_port)||(temp->src_ip==(new_row)->dst_ip&&
                                                        temp->dst_ip==(new_row)->src_ip&&
                                                        temp->src_port==(new_row)->dst_port&&
                                                        temp->dst_port==(new_row)->src_port))
            {//found
                return temp;
            }
            
        }
        temp=temp->next;
        
    }
    return NULL;
}
//loop on all of the connections
//the connections that we are trying to delete:
//connections that there mode are still on handshake and there time is longer then 25 seconds.
//connections that there are in the tcp fin mode , and the last ack just arrived,
static __u8  try_delete_connections(log_row_t * new_row){
    log_conn_t *temp;
    
    struct timeval timestamp;
    unsigned long curr_time;
    do_gettimeofday(&timestamp);
    curr_time=timestamp.tv_sec;
    temp=list_conn->head;
    while(temp!=NULL)
    {   //this mean the can be deleted.
        if(temp->timestamp>0)
        {
            //check if this connection time expired
            if (curr_time-temp->timestamp>25) {
                delete_connection(temp);
            }
            //find if connection is last stage of fin ,(second fin)
            if ((temp->src_ip==(new_row)->src_ip&&
                 temp->dst_ip==(new_row)->dst_ip&&
                 temp->src_port==(new_row)->src_port&&
                 temp->dst_port==(new_row)->dst_port) &&(temp->state==END_CLIENT))
            {
                new_row->reason=SERVICE;
                delete_connection(temp);
                return NF_ACCEPT;
            }
            //find if connection is last stage of fin ,(second fin)
            if ((temp->src_ip==(new_row)->dst_ip&&
                 temp->dst_ip==(new_row)->src_ip&&
                 temp->src_port==(new_row)->dst_port&&
                 temp->dst_port==(new_row)->src_port) &&(temp->state==END_SERVER))
            {
                new_row->reason=SERVICE;
                delete_connection(temp);
                return NF_ACCEPT;
                
            }
        }
        //this connection is ok try go to another
        temp=temp->next;
    }
    return NF_DROP;
}


//if the timestamp of the connection is above 0 it means the connection did not finish the handshake mode,
//we try to find if there is a connection form that type in the list that match the source and dest ip and port
static log_conn_t * find_conn_in_table_handshake(log_row_t * new_row){
    log_conn_t *temp;
    
    
    temp=list_conn->head;
    while(temp!=NULL)
    {   //this mean we are still on handshake
        if(temp->timestamp>0)
        {
            
            if ((temp->src_ip==(new_row)->src_ip&&
                 temp->dst_ip==(new_row)->dst_ip&&
                 temp->src_port==(new_row)->src_port&&
                 temp->dst_port==(new_row)->dst_port)||(temp->src_ip==(new_row)->dst_ip&&
                                                        temp->dst_ip==(new_row)->src_ip&&
                                                        temp->src_port==(new_row)->dst_port&&
                                                        temp->dst_port==(new_row)->src_port))
            {//found
                return temp;
            }
            
        }
        temp=temp->next;
        
    }
    return NULL;
}
//try find a connection in the list with port and ip source and dest opposite to the packet that just arrived
static log_conn_t * find_opposit_connection(log_row_t * new_row){
    log_conn_t *temp;
    temp=list_conn->head;
    while(temp!=NULL)
    {
        if (temp->src_ip==(new_row)->dst_ip&&
            temp->dst_ip==(new_row)->src_ip&&
            temp->src_port==(new_row)->dst_port&&
            temp->dst_port==(new_row)->src_port)
        {//found
            return temp;
        }
        
        
        temp=temp->next;
    }
    return NULL;
}
//find if packet is match a rule from the rule table, if accept the packet with the reason of the number of rule.
//in addition if the packet is from protocol tcp open new handshake connection ,and start timer of 25 seconds
static __u8 handle_static_inspection(const struct net_device *in,
                                     const struct net_device *out,log_row_t* new_row)
{
    __u8 check_result;
    log_conn_t* conn;
    check_result=inspect_row_static(in,out,new_row);
    //if connection is tcp, and found in static, we need to verify handshake, create new row in table handshake
    if((new_row)->protocol==PROT_TCP &&check_result==NF_ACCEPT)
    {
        if((new_row)->syn==1)
        {  conn=find_conn_in_table_handshake(new_row);
            if(conn==NULL)//if connection not found open new
                conn=generate_new_connection(new_row);
            check_result=NF_ACCEPT;
            if(conn==NULL)
            {
                clean_all();
                return ERROR;
            }
        }
        
    }
    return check_result;
}
//if we find connection match src ip and port 0 and the connection is defined as HTTP_PROT ,
// this is the first time we ask for tcp connection for the redirect. change the ip and port to be as the packet recived.
static __u8 find_conn_in_table_for_redirect(log_row_t* new_row)
{
    __be16 src_port;
    __be32 src_ip;
    log_conn_t* handshake_conn;
    if((new_row)->tcp_flags==ACK_NO&&(new_row)->syn==1)
    {src_port=new_row->src_port;
        src_ip=new_row->src_ip;
        new_row->src_ip=0;
        new_row->src_port=0;
        handshake_conn=find_conn_in_table_handshake(new_row);
        new_row->src_port=src_port;
        new_row->src_ip=src_ip;
        if(handshake_conn!=NULL)
            if(handshake_conn->state==READY_TO_NEW &&handshake_conn->protocol==HTTP_PROT)
            {
                handshake_conn->state=TCP_SYN;
                handshake_conn->src_port=src_port;
                handshake_conn->src_ip=src_ip;
                new_row->reason=SERVICE;
                return NF_ACCEPT;
            }
    }
    return NF_DROP;
}
//if connection is still not open , we inspect the packets of the seesion to be in the right direction and form of tcp handsake (we inspect the stages of -> syn, <- syn ack, -> ack
static __u8 handle_verify_handsake(log_row_t* new_row){
    log_conn_t* opp_conn;
    log_conn_t* handshake_conn;
    new_row->reason=SERVICE;
    handshake_conn=find_conn_in_table_handshake(new_row);
    opp_conn=find_opposit_connection(new_row);
    if((new_row)->tcp_flags==ACK_NO&&(new_row)->syn==1)
    {
        if(handshake_conn!=NULL)
            if(handshake_conn->state==READY_TO_NEW)
            {//this is the first time of the connection of the tcp
                handshake_conn->state=TCP_SYN;
                return NF_ACCEPT;
            }
    }
    if((new_row)->syn==1&&(new_row)->tcp_flags==ACK_YES)
    {
        if( opp_conn!=NULL)
            if(opp_conn->state==TCP_SYN)
            {opp_conn->state=TCP_SYN_ACK;
                return NF_ACCEPT;
            }
        if(handshake_conn!=NULL)
            if(handshake_conn->state==TCP_SYN_ACK)
                return NF_ACCEPT;
    }
    if((new_row)->tcp_flags==ACK_YES&&(new_row)->syn==0)
    {
        if(handshake_conn!=NULL)
            if(handshake_conn->state==TCP_SYN_ACK)
            {//finsh handaske deal with protocols
                handshake_conn->state=OPEN_CONN;
                handshake_conn->timestamp=0;
                return NF_ACCEPT;
            }
    }
    
    //if we reached here this is not a connection in a handshake mode
    new_row->reason=REASON_ILLEGAL_VALUE;
    return NF_DROP;
}

//get data from packet of tcp
static unsigned char * get_tcp_data(struct sk_buff *skb){
    struct tcphdr * tcp_hdr;
    struct iphdr *ip_header;
    ip_header = (struct iphdr *)skb_network_header(skb);
    tcp_hdr = (struct tcphdr *)(skb->data +(ip_header->ihl * 4));
    if(!tcp_hdr) return NULL;
    return (unsigned char *)((unsigned char *)tcp_hdr + (tcp_hdr->doff * 4));
    
}
//if connection is register as ftp , we parse the data to find that first the connection astablished succesfuly (form the server the 230 command send it meand the client hase been approved) and after that we wait for the  get command with the string of PORT.
static __u8 handle_ftp_protocol(char * data,log_row_t* new_row,log_conn_t* dynamic_conn)
{    char * port;
    char first[5];
    char sec[5];
    char* first_end_place;
    char* sec_end_place;
    const int port_mess_size=14;
    long pf;
    long ps;
    char **temp;
    long new_port;
    const long int base=10;
    log_conn_t *conn;
    log_conn_t* opp_conn;
    temp=NULL;
    opp_conn=find_opposit_connection(new_row);
    if(dynamic_conn->state==FTP_READY_TO_CLIENT)
    {
        //server replied that he accept the user. we are now ready to handle PORT requests
        if((opp_conn!=NULL)&& (NULL!=strstr(data,"230")))
            dynamic_conn->state=FTP_ACCEPT_CLIENT;
        
    }
    if(dynamic_conn->state==FTP_ACCEPT_CLIENT)
    {
        port=strstr(data,"PORT 10,0,1,1,");
        if(port!=NULL)
        {//got PORT command ,parse the data to open the new connection in the right port
            port=port+port_mess_size;
            first_end_place=strstr(port,",");
            memcpy( first, port,first_end_place-port);
            first[first_end_place-port]='\0';
            
            
            pf = simple_strtol(first, temp, base);
            
            sec_end_place=strstr(first_end_place,"\n");
            memcpy( sec, first_end_place+1,sec_end_place-first_end_place-1);
            sec[sec_end_place-first_end_place-1]='\0';
            
            ps = simple_strtol(sec, temp, base);
            
            new_port=pf*256+ps;
            printk("ftp get command numbers: first = %lu ,secong = %lu general=%d \n",pf,ps,(int)new_port);
            conn=generate_new_ftp_connection(new_row,(int)new_port);
            if(conn==NULL)
            {
                clean_all();
                return ERROR;
            }
        }
    }
    return NF_ACCEPT;
}
//after the GET command has been seen the connection is open as HTTP, we inspect the connection if there is a redirect answer form the server we open new connection
static __u8 handle_http_protocol(char * data,log_row_t* new_row,log_conn_t* dynamic_conn)
{
    log_conn_t *conn;
    char *http;
    char *http_end;
    char addresport[30];
    log_conn_t* opp_conn;
    opp_conn=find_opposit_connection(new_row);
    if(dynamic_conn->state==HTTP_GET_REQUEST &&(opp_conn!=NULL))
    { http=strstr(data,"HTTP/1.1 3");
        if(http!=NULL)
        {
            http=strstr(http,"http");
            http+=7;
            http_end=strstr(http,"\n");
            memcpy( addresport, http,http_end-http-1);
            addresport[http_end-http -1]='\0';
            printk("redirect to=%s\n",addresport);
            conn=generate_new_http_connection(new_row,addresport);
            if(conn==NULL)
            {
                clean_all();
                return ERROR;
            }
            
        }
    }
    return NF_ACCEPT;
}
///
//[truncated] GET /zabbix/httpmon.php?applications=2%20AND%20%28SELECT%201%20FROM%28SELECT%20COUNT%28%2a%29%2cCONCAT%280x71505a61776f7146%2c%28SELECT%20MID%28%28IFNULL%28CAST%28sessionid%20AS%20CHAR%29%2c0x20%29%29%2c1%2c50%29%20FROM%20zabbi

//if we are in tcp connection after handshake done, the port and if the connection on the relevant port with the relevant data register the relevant protocol to the connection
static __u8 try_find_protocol(char * data,log_row_t* new_row,log_conn_t* dynamic_conn)
{
    log_conn_t* opp_conn;
    int size;
    char *temp;
    char *temp_endl;
    char ap[31];
    int size_to_copy;
    unsigned long temp_number;
    opp_conn=find_opposit_connection(new_row);
    
    //check for zabbix exploit
    if(strstr(data,"GET /zabbix/httpmon.php?applications=")!=NULL)
    {   temp=strstr(data,"=");
        temp++;
        temp_endl=strstr(data,"\n");
        size_to_copy=temp_endl-temp;
        if(size_to_copy>30)
            size_to_copy=30;
        memcpy( ap, temp,size_to_copy);
        ap[size_to_copy]='\0';
        printk("size_to=%d zabbix message is-%s\n",size_to_copy,ap);
        
        
        if(0!=strict_strtoul(ap,10,&temp_number))
        {
            new_row->reason=ZABBIX;
            printk("dropped due ZABBIX\n");
            dynamic_conn->timestamp=1;
            dynamic_conn->state=TCP_SYN;
            return NF_DROP;
        }
        
    }
    
    
    if(new_row->src_port==21)
    {
        if(NULL!=strstr(data,"220")&&(opp_conn!=NULL))
        { //server redy to accept client
            dynamic_conn->state=FTP_READY_TO_CLIENT;
            dynamic_conn->protocol=FTP_PROT;
            printk("220 found going to FTP\n");
        }
        
    }
    if(new_row->dst_port==80)
    {
        if((NULL!=strstr(data,"GET"))&&(opp_conn==NULL))
        {//client asks for get
            temp=strstr(data,"HTTP");
            if(temp!=NULL)
            {   size=strlen(data)-strlen(temp)-6;
                if(size>64)
                {
                    new_row->reason=WEBSTER;
                    printk("dropped due WEBSTER exploit(url above 64)\n");
                    dynamic_conn->timestamp=1;
                    dynamic_conn->state=TCP_SYN;
                    return NF_DROP; // this protects the buffer overflow
                }
                
                dynamic_conn->protocol=HTTP_PROT;
                dynamic_conn->state=HTTP_GET_REQUEST;
                printk("get found going to HTTP\n");
            }
        }
        
        
    }
    return NF_ACCEPT;
}


static int get_number_chars_to_end_line(char * data,int start)
{
    int i;
    
    for(i=0;i<strlen(data);i++)
        if(*(data+i)==10)
            return i;
    
    return i;
    
}



//return place of first occurrence of ch found in data in range of[start-end]
//else return -1
//start counting from 0
static int get_first_word(char * data,char ch[],int start,int end){
    int place;
    char *tempData;
    char *startData;
    if(strlen(data)<=start||end<0)
        return -1;
    if(end>strlen(data))
        end=strlen(data);
    tempData=data+start;
    startData=strstr(tempData,ch);
    if(startData!=NULL)
    {
        place= strlen(data) -strlen(startData);
        if(place>end)
            return -1;
        else return place;
    }
    return -1;
}

static int split_to_words(char* tempData,char* arr[],int arr_size)
{
    int start;
    int end_word;
    int i;
    start=0;
    for(i=0;i<arr_size;i++)
    {
        end_word=get_first_word(tempData,"#",start,strlen(tempData));
        if(end_word<0)
            return i;
        strncpy(arr[i],tempData+start,end_word+1);
        *(arr[i]+(end_word-start))='\0';
        start=end_word+1;
    }
    return i;
}


//return the times of occurrence of ch multiply by avg_weigh
static int scan_for_tav(char * data,char ch[],int start,int end,int avg_weight){
    int times;
    int place;
    times=0;
    
    place= get_first_word(data,ch,start,end);
    while (place>=0) {
        times++;
        place= get_first_word(data,ch,place+1,end);
    }
    return times*avg_weight;
}

//return  if ch found in data in range of [start-end]
//start counting from 0
static int get_last_word(char * data,char ch[],int start,int end){
    int place;
    int tempPlace;
    
    place=-1;
    for(tempPlace=start;tempPlace<=end;tempPlace++)
    {
        
        if(get_first_word(data,ch,tempPlace,end)>0)
            place=get_first_word(data,ch,tempPlace,end);
    }
    return place;
}

//return place of last occurrence of ch found in data in range of[start-end]
//else return -1
//start counting from 0
static bool is_word_prev(char * data,char chA[],char chB[],int start,int end){
    int place_caA;
    int place_caB;
    place_caA= get_first_word(data,chA,start,end);
    place_caB= get_first_word(data,chB, place_caA+1,end);
    if(place_caA<0||place_caB<0)
        return false;
    if(place_caA>=place_caB)  //if it == this is the only occurrence of chB and chA
        return false;
    return true;
}


//return place of last occurrence of ch found in data in range of[start-end]
//else return -1
//start counting from 0
static int get_number_of_words(char * data,int start,int end){
    int currPlace;
    int numOfWords;
    bool serch_space;
    int i;
    numOfWords=0;
    currPlace=0;
    
    if(*(data+start)==' ')
    {
        serch_space=true;
        
    }
    else
    {
        serch_space=false;
        
    }
    for(i=start;i<end;i++ )
    {
        if((*(data+i)!=' ')&&(*(data+i)!=10)&&serch_space==false) //found char
        {
            numOfWords++;
            serch_space=true;
        }
        if(((*(data+i)==' ')||(*(data+i)==10))&&serch_space==true) //found char
        {
            serch_space=false;
        }
        
    }
    
    return numOfWords;
}


//inspect line by line macros include and define,
//define have pattern of #define word word
//include have pattern of #include word.h
static int weight_macros(char * data){
    int    end_line;
    int start_line;
    int weight;
    start_line=0;
    weight=0;
    end_line=get_first_word(data,"\n",start_line,strlen(data));
    while (end_line>0)
    {
        if(is_word_prev(data,"#define"," ",start_line,end_line))
        {
            if(get_number_of_words(data,start_line,end_line)==3)
            {
                //it means we found pattern of #define word word
                weight+=get_number_of_words(data,start_line,end_line)*5;
            }
        }
        if(is_word_prev(data,"#include",".h",start_line,end_line)||is_word_prev(data,"#include",".c",start_line,end_line))
        {
            if(is_word_prev(data," ",".h",start_line,end_line)||is_word_prev(data," ",".c",start_line,end_line))
            {
                if(get_number_of_words(data,start_line,end_line)==2)
                    //it means we found pattern of #include word.h
                    weight+=get_number_of_words(data,start_line,end_line)*5;
            }
        }
        start_line=end_line+1;
        end_line=get_first_word(data,"\n",start_line,strlen(data));
    }
    
    return weight;
}




//return the first place of the last word in the pattern array
static int find_patern(char * data,int start,int end,char *pattern[],int sizeArr)
{
    int i;
    for(i=0;i<sizeArr;i++)
    {
        start=get_first_word(data,pattern[i],start,end);
        if(start<0) //if pattern[i] dont found, start is -1
            return start;
        start+=1;
        
    }
    return start;
}

static bool is_code_depth_legal(char * data,int start,int end)
{
    int depth;
    int i;
    depth=0;
    if (start<0 || end>strlen(data))
        return false;
    for(i=start;i<=end;i++ )
    {
        if(*(data+i)=='{')
        {
            depth++;
            
            
        }
        if(*(data+i)=='}')
        {
            depth--;
            
        }
        
        if(depth<0)//illegal pattern more } then {
            return false;
        
    }
    if(depth==0)
    {
        
        return true;
    }
    return false;  //more { then }
}

//serch for pattern tempPrintf in range [start_place,end_place] , if not found return -1
static int weight_pttern(char *data,int start_place,int* weight,char* tempPrintf)
{
    int num_of_words;
    const int arr_size=5;
    char *arr[arr_size];
    char arrtemp1 [30];
    char arrtemp2 [30];
    char arrtemp3 [30];
    char arrtemp4 [30];
    char arrtemp5 [30];
    int end_place;
    arr[0]=arrtemp1;
    arr[1]=arrtemp2;
    arr[2]=arrtemp3;
    arr[3]=arrtemp4;
    arr[4]=arrtemp5;
    
    num_of_words=split_to_words(tempPrintf,arr,arr_size);
    //all patterns are in the form of word then "(", verify the ( is found up to 3 chars from the first word of the pattern
    end_place=get_first_word(data,"(",start_place,start_place+strlen(arr[0])+3);
    
    if(end_place>=0)
    {  //serch the pattern until end of line
        end_place=get_number_chars_to_end_line(data,start_place)+start_place+2;
        
        end_place=find_patern(data,start_place,end_place,arr,num_of_words);
        
        if(!(end_place<0))
        {//pattern found
            
            *weight+=get_number_of_words(data,start_place,end_place);
            
        }
    }
    return end_place;
}





//inspect patterns of for(#;#;#) if(#) while(#)
//if found inside the statement a logical pattern increase the weight
static int weight_flow(char * data,int start,int end){
    int place;
    int end_place;
    int place_if;
    int place_for;
    int place_while;
    char tempFor [30]="for#(#;#;#)#\0";
    char tempif [30]="if#(#)#\0";
    char tempwhile [30]="while#(#)#\0";
    int p1;
    int p;
    int weight;
    int start_place;
    weight=0;
    place=start;
    start_place=start;
    end_place=-1;
    while(place>=0)
    {
        
        place_if=get_first_word(data,"if(",place,end);
        place_for=get_first_word(data,"for(",place,end);
        place_while=get_first_word(data,"while(",place,end);
        //if nothing found brake;
        if(place_if<0 && place_for<0 && place_while<0)
            break;
        //if one not found his place is bigger than length so we will not search for it
        if(place_if<0)
            place_if=strlen(data)+1000;
        if(place_for<0)
            place_for=strlen(data)+1000;
        if(place_while<0)
            place_while=strlen(data)+1000;
        //serch for
        if(place_for<place_if && place_for<place_while)
        {
            
            start_place=place_for;
            end_place=weight_pttern(data,start_place,&weight,tempFor);
            
        }
        //while
        if(place_while<place_if && place_while<place_for)
        {
            start_place=place_while;
            end_place=weight_pttern(data,start_place,&weight,tempwhile);
            
        }
        //if
        if(place_if<place_while && place_if<place_for)
        {
            start_place=place_if;
            end_place=weight_pttern(data,start_place,&weight,tempif);
            
        }
        
        
        while (start_place>=0&&start_place<end_place){
            p=end_place;
            //search for patterns in the statements
            p1=get_first_word(data,"&&",start_place,end_place);
            p=(p1<p)&&(p1>0)?p1:p;
            p1=get_first_word(data,"==",start_place,end_place);
            p=(p1<p)&&(p1>0)?p1:p;
            p1=get_first_word(data,"<",start_place,end_place);
            p=(p1<p)&&(p1>0)?p1:p;
            p1=get_first_word(data,">",start_place,end_place);
            p=(p1<p)&&(p1>0)?p1:p;
            p1=get_first_word(data,"||",start_place,end_place);
            p=(p1<p)&&(p1>0)?p1:p;
            
            if(p!=end_place)
            { weight++;
                place=p+1;
            }
            
            start_place=p+1;
            
        }
        
        
        place=end_place;
        
    }
    
    return weight;
}


//inspect patterns of for#(#;#;#) if#(#) while#(#)
//if found inside the statement a logical pattern increase the weight
static int weight_known_pattern(char * data,int start,int end){
    
    
    int place;
    int end_place;
    int place_printf;
    int place_scanf;
    char tempPrintf [30]="print#(#\"#\"#);#\0";
    char tempScanf [30]="scan#(#\"#\"#);#\0";
    char n[3]={'\\','n','\0'};
    int p;
    int p1;
    int weight;
    int start_place;
    weight=0;
    
    place=start;
    start_place=0;
    end_place=-1;
    while(place>=0)
    {
        
        place_printf=get_first_word(data,"print",place,end);
        place_scanf=get_first_word(data,"scan",place,end);
        //if nothing found brake;
        if(place_printf<0 && place_scanf<0 )
            break;
        //if one not found make his place bigger than length so we will not search for it
        if(place_printf<0)
            place_printf=strlen(data)*3;
        if(place_scanf<0)
            place_scanf=strlen(data)*3;
        // scanf
        if(place_scanf<place_printf)
        {
            start_place=place_scanf;
            end_place=weight_pttern(data,start_place,&weight,tempScanf);
        }
        //printf
        if(place_printf<place_scanf)
        {
            
            start_place=place_printf;
            end_place=weight_pttern(data,start_place,&weight,tempPrintf);
            
        }
        
        //search for patterns in the statements
        
        while (start_place>=0&&start_place<end_place)
        {  p=end_place;
            //search for patterns in the statements
            p1=get_first_word(data,n,start_place,end_place);
            p=(p1<p)&&(p1>0)?p1:p;
            p1=get_first_word(data,"%d",start_place,end_place);
            p=(p1<p)&&(p1>0)?p1:p;
            p1=get_first_word(data,"%s",start_place,end_place);
            p=(p1<p)&&(p1>0)?p1:p;
            
            if(p!=end_place)
            { weight++;
                place=p+1;
            }
            
            start_place=p+1;
            
        }
        place=end_place>0?end_place:start_place+1;
        
    }
    
    return weight;
}

/*
 2.
 each line with the pattern print#(#“#”); or scan#(#“#”); will be count as the number of words the pattern took from start to end.
 If there are inside the pattern %d %s /n I increase the weight by one for each
 the code of this rule is found in weight_known_pattern function
 3.
 Each line with the pattern if#(#) for#(#;#;#) while#(#) will be count as the number of words the pattern took from start to end.
 If  there are inside the pattern == <&& || > I increase the weight by one for each
 the code of this rule is found in weight_flow function
 4.
 I weight each “;” I found as 1 and each “->” as 2
 
*/

static int find_pice_of_flow(char * data)
{
    int start_place;
    int end_place;
    int weight;
    int temp;
    int end;
    start_place=0;
    weight=0;
    temp=0;
    end=strlen(data);
    end_place=end;
    weight+=weight_flow(data,start_place,end_place);
    weight+=weight_known_pattern(data,start_place,end_place);
    
    weight+=scan_for_tav(data,"->",0,end_place,2);
    
    weight+=scan_for_tav(data,";",0,end_place,1);
    for (end_place=0; end_place<strlen(data); end_place++)
    {
        
        
        if (*(data+end_place)=='}')
        {
            start_place=end_place-1;
            while(is_code_depth_legal(data,start_place,end_place)==false)
            {
                
                start_place--;
                if (start_place<0) {
                    break; //illegal pattern
                }
            }
            
            if(is_code_depth_legal(data,start_place,end_place))
            {
                
                weight+=scan_for_tav(data,"->",start_place,end_place,2);
                
                weight+=scan_for_tav(data,";",start_place,end_place,1);
                weight+=weight_flow(data,start_place,end_place);
                weight+=weight_known_pattern(data,start_place,end_place);
            }
        }
        
    }
    return weight;
}





/*
 the DLP for C code inspection is measured between ratio of number of words in the packet and the weight defined
 if the weight is higher than the number of words, the packet will be dropped, and logged as DLP, and the connection will be deleted
 
 each weight form the weight rules will be multiple by the number of brackets (“ { }”) surround it +1
 
wight rules:
1.
I search for patterns #include and #define in the following manner,
#include -have to be with 2 words, and ends with .h or .c at end of the second word
#define- have to be with 3 words.
I weight each #include and #define as the number of words they have multiply by 5
the code of this rule is found in weight_macros function
2.
each line with the pattern print#(#“#”); or scan#(#“#”); will be count as the number of words the pattern took from start to end.
If there are inside the pattern %d %s /n I increase the weight by one for each
the code of this rule is found in weight_known_pattern function
3.
Each line with the pattern if#(#) for#(#;#;#) while#(#) will be count as the number of words the pattern took from start to end.
If  there are inside the pattern == <&& || > I increase the weight by one for each
the code of this rule is found in weight_flow function
4.
I weight each “;” I found as 1 and each “->” as 2

*/

static __u8 dlp_inspection(char * data){
    int weight;
    
    weight=0;
    
    weight+=weight_macros(data);
    weight+=find_pice_of_flow(data);
    //  printk("weight=%d num of words=%d data=\n%s\n",weight,get_number_of_words(data,0,strlen(data)),data);
    printk("weight=%d num of words=%d \n",weight,get_number_of_words(data,0,strlen(data)));
    if (weight>get_number_of_words(data,0,strlen(data)))
        return NF_DROP;
    return NF_ACCEPT;
}
//this function called only on open connections (that have been approved) the function parse the tcp data and ports handle the different protocols
static __u8 handle_protocol_connection (struct sk_buff *skb,log_row_t* new_row,log_conn_t* dynamic_conn)
{
    char * data;
    //this packet is on service,
    (new_row)->reason=SERVICE;
    
    data= get_tcp_data(skb);
    if(dlp_inspection(data)==NF_DROP)
    {
        printk("dropped due DLP inspection\n");
        dynamic_conn->timestamp=1;
        dynamic_conn->state=TCP_SYN;
        (new_row)->reason=DLP;
        return NF_DROP;
    }
    
    
    if(dynamic_conn->protocol==PROT_TCP)
    {
        return try_find_protocol(data,new_row,dynamic_conn);
    }
    
    if(dynamic_conn->protocol==FTP_PROT)
    {
        return handle_ftp_protocol(data,new_row,dynamic_conn);
    }
    
    if(dynamic_conn->protocol==HTTP_PROT)
    {
        return handle_http_protocol(data,new_row,dynamic_conn);
    }
    //this is an approved connection accept it
    return NF_ACCEPT;
    
}


//if there is fin flag in the packet, we forward the connection in the stages of the tcp
//if it is the second fin, in the right direction we open timer to delete the tcp connection
static __u8 handle_fin(log_row_t* new_row)
{
    log_conn_t* opp_conn;
    log_conn_t* handshake_conn;
    struct timeval timestamp;
    handshake_conn=find_conn_in_table_dynamic(new_row);
    opp_conn=find_opposit_connection(new_row);
    (new_row)->reason=SERVICE;
    //it means the timer is open the connection can be deleted
    if(handshake_conn!=NULL)
    {//if we got second fin in the other direction
        if((handshake_conn->state==END_SERVER&&opp_conn==NULL) ||(handshake_conn->state==END_CLIENT&&opp_conn!=NULL))
        {
            do_gettimeofday(&timestamp);
            (handshake_conn)->timestamp=timestamp.tv_sec;
            return NF_ACCEPT;
        }
        
        //if the connection is in open and we got fin , forward the connection in state depend to the direction the fin came from
        if(handshake_conn->state>=OPEN_CONN)
        {if( opp_conn!=NULL)
            handshake_conn->state=END_SERVER;
        else
            handshake_conn->state=END_CLIENT;
            return NF_ACCEPT;
        }
    }
    (new_row)->reason=REASON_ILLEGAL_VALUE;
    return NF_DROP;
}
//enter the action to the packet and log the packet,
//decide by check_result if to accept the packet
static __u8 end_of_hook(log_row_t* new_row,__u8 check_result)
{
    //we got error we don't log the row we exit the hook
    if (check_result==ERROR)
        return NF_DROP;
    (new_row)->action=check_result;
    if(is_row_new(new_row)==ERROR)
    {
        clean_all();
        return NF_DROP;
    }
    kfree(new_row);
    printk("end hook %d\n\n\n\n",check_result);
    return check_result;
}

/*
 hook that inspect packets in the NF_INET_FORWARD stage, each packet goes to the following stages:
 1)in the function generate_log_row I fill the last place of the array row with the packets details
 2)each packet goes to the end of the hook with the function end_of_hook, there I log the packet with the relative reason and action that will be applied on this packet.
 3)check for basic rules -each packet first enter this function that checks the following possibilities:
 3.1)christmas tree packet: TCP packet with the flags SYN, FIN, URG and PSH that can used in a DOS attack because of  require more processing, this packet will be dropped with the reason  REASON_XMAS_PACKET (enum -4)
 3.2)fw not active: if the FW is not active and just registered in the kernel, it will log the packets but will accept all of them with the reason  REASON_FW_INACTIVE (enum -1)
 3.3)basic rule: if it is a packet from the FW to itself we don't log the packet and accept it.
 4)try_delete_connections-each time packet gets to the hook we go on through all of the connections that are now on the tcp handshake ,or on at the end of the TCP connection,
 4.1)connection that is on handshake state and its time passed.
 4.2)connection on that previously got FIN from one side (will be on states END_CLIENT/END_SERVER depend on the side that sends the FIN) and the packet is the last packet of the connection ,we delete the connection and end the hook with accept .
 5)packet with FIN flag-each packet session with state >2 (OPEN_CONN state and above) will forward to the stages of the tcp termination
 6)packet with ACK flag-
 6.1)if the packet is from open and known session , the packet will be accept and we handle the packet in the relevant protocol it belongs to in the handle_protocol_connection function. else if the 6.2)if it is not from a known connection the packet will be checked if it is part of the handshake of a session
 6.3)else it is a packet with a ACK that we did not approved and we drop it with reason of illegal value.
 7)packet without ACK and no FIN -
 7.1)we check if the packets is part of a new dynamic connection we try to establish after FTP get.
 7.2)if not ,we try to search if the packet is part of the new HTTP redirect request .
 7.3)else we check if packet is valid in the static rule table, there if the packet is valid and its TCP protocol and got syn we open new handshake tcp session
 
 
 */

static unsigned int hook_accept(unsigned int hooknum,
                                struct sk_buff *skb,
                                const struct net_device *in,
                                const struct net_device *out,
                                int (*okfn)(struct sk_buff *)){
    __u8 check_result;
    log_row_t* new_row;
    log_conn_t* dynamic_conn;
    printk(" start hook\n");
    new_row= generate_log_row(skb,hooknum);
    if(new_row==NULL)
    {
        clean_all();
        return NF_DROP;
    }
    printk(" src %pI4 %d\n",&new_row->src_ip, new_row->src_port);
    printk(" dst %pI4 %d\n",&new_row->dst_ip, new_row->dst_port);
    
    
    /** check for basic rules (xmas/fw not active/basic)  */
    
    //return NF_DROP in case didnt find connection/static rule that will change in the midule will not change
    check_result=inspect_row_basic(in,out,new_row);
    // if its basic rule ,dont log it.
    if ((new_row)->reason==BASIC_RULE)
        return check_result;
    //if row xmas/fw not active/basic rule end of hook
    if((new_row)->reason!=0)
    {//we found a reson to end the hook (or XMS or fw_not_active) the reson was set in the function inspect_row_basic.
        return end_of_hook(new_row,check_result);
    }
    
    
    /** try delete connection and check if the packet is last ack of tcp connection (after fin)  */
    check_result= try_delete_connections(new_row);
    if(check_result==NF_ACCEPT)
    {//means the packet last ack of connection of tcp, accept it and log it as a servise
        return end_of_hook(new_row,NF_ACCEPT);
    }
    
    /**  handle fin packets, if it is the second fin (the oposite direction of the first fin) oppen timer for the last ack of the connection */
    
    if((new_row)->tcp_flags==FIN)
    {
        check_result= handle_fin(new_row);
        return end_of_hook(new_row,check_result);
    }
    /** packet got ack, check for the dynamic table, or check if it is still on the handshake mode */
    if((new_row)->tcp_flags==ACK_YES)
    {
        dynamic_conn=find_conn_in_table_dynamic(new_row);
        if(dynamic_conn!=NULL)
            //connection found in the dynamic (after handshake no need to check that). handle protocol things
        {
            check_result=handle_protocol_connection(skb,new_row,dynamic_conn);
        }
        else
            //check if it is still on the handshake mode
        {
            check_result=handle_verify_handsake(new_row);
        }
        
        return end_of_hook(new_row,check_result);
    }
    /**packet didn't get ack and didn't get final. check it if it is the first time hand shake ,it can be in the static rules/first time of handshake in connection we just open */
    
    if((new_row)->tcp_flags!=ACK_YES&&(new_row)->tcp_flags!=FIN)
        
    {
        
        dynamic_conn=find_conn_in_table_handshake(new_row);
        if(dynamic_conn!=NULL)
            //connection found in the handshake stage and we don't got ack , means this is the first time of the ftp get data response
        {
            check_result=handle_verify_handsake(new_row);
            return end_of_hook(new_row,check_result);
        }
        
        //try serch connection if it is http redirect.
        check_result=find_conn_in_table_for_redirect(new_row);
        if(check_result==NF_ACCEPT)//it is redirec reason=service
            return end_of_hook(new_row,check_result);
        //if it is not first time of redirect or FTP get  serch in static table.
        //if the conneciton is tcp and found in the static table , open new dynamic connection
        check_result=handle_static_inspection(in,out,new_row); //row reason will be the the number of the rule static table,reason set in the handle static
        
        //end hook
        return end_of_hook(new_row,check_result);
    }
    return NF_DROP;
}


//static functions to register the hook
static void nf_register_hook_local_out(void){
    nfLoacl_out.hook = hook_accept;
    nfLoacl_out.hooknum =  NF_INET_FORWARD;
    nfLoacl_out.pf = PF_INET;
    nfLoacl_out.priority = NF_IP_PRI_FIRST;
    nf_register_hook(&nfLoacl_out);
}
/*this method will be invoked in the module_init/
 the method creat the devices and the atributes ,fill the baisic rule (rule 0)
 and registers the hooks in the forward stage*/
static int __init init_main(void) {
    int errSysfs;
    list_conn=kmalloc(sizeof(list_conn_t), GFP_ATOMIC);
    if(list_conn==NULL)
        return ERROR;
    list_conn->head=NULL;
    list_conn->tail=NULL;
    conn_read=list_conn->head;
    rule_table = kmalloc(sizeof(rule_t)*(MAX_RULES+1), GFP_ATOMIC);//+1 for the baisic rule
    if(rule_table==NULL)  { kfree(list_conn);return ERROR;}
    row = kmalloc(sizeof(log_row_t)*size_row_table,GFP_ATOMIC);
    if(row==NULL)
    {kfree(rule_table); kfree(list_conn);return ERROR; }
    errSysfs = register_sysfs_device();
    printk("register device\n");
    if (errSysfs != SUCCESS)
    {kfree(row); kfree(rule_table); return errSysfs; }
    nf_register_hook_local_out();
    printk("register hoks\n");
    return fill_basic_rule();
}
void destroy_connection_list(void){
    log_conn_t *temp;
    while(list_conn->head!=list_conn->tail)
    {
        temp=list_conn->head;
        list_conn->head=list_conn->head->next;
        kfree(temp);
        
    }
    kfree(list_conn->head);
    list_conn->head=NULL;
}

/*this method will be invoked in the module_exit and on errors on the mudel
 the method unregistered the hooks and the devices free memeory of the log and rule_table */
static void clean_all(void){
    printk ("clean all\n");
    destroy_connection_list();
    kfree(list_conn);
    if (rule_table!=NULL)
    {
        kfree(row);
        row=NULL;
        kfree(rule_table);
        rule_table=NULL;
        nf_unregister_hook(&nfLoacl_out);
        printk("hook unregistered\n");
        unregister_sysfs_device();
        printk("device unregistered\n");
    }
}


module_init(init_main);
module_exit(clean_all);